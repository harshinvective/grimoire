WordUtilities = function() {

	var wordLists = Object.freeze({
		'adjective': wordList.adjective,
		'noun': wordList.noun,
		'verb': wordList.verb,
		'abstractNoun': wordList.abstractNoun,
	});

	var getRandomNoun = function() {
		return Utilities.randomElement(wordLists.noun).word;
	}

	var getRandomAdjective = function() {
		return Utilities.randomElement(wordLists.adjective).word;
	}

	var getRandomVerb = function() {
		return Utilities.randomElement(wordLists.verb).word;
	}	

	var capitalizeWord = function(word) {
		word = word.charAt(0).toUpperCase() + word.slice(1);
		for (var i = 1; i < word.length - 1; i++) {
			if (word.charAt(i).match(/[^A-Za-z']/i)) {
				word = word.substring(0, i+1) + word.charAt(i+1).toUpperCase() + word.slice(i+2);
			}
		}
		return word;
	}

	var buildWordMaps = function(wordList) {
		var wordMaps = {
			'adjective': {},			
			'noun': {},
			'verb': {},
			'abstractNoun': {}
		}

		for(partOfSpeech in wordList) {
			var list = wordList[partOfSpeech];
			var map =  wordMaps[partOfSpeech];

			map.color = {};
			GeneratorUtilities.getColorCodeList().forEach(function(color) {
				map.color[color] = [];
			});

			list.forEach(function(word) {
				if (word.category) {
					for(categoryName in word.category) {
						if (!(categoryName in map)) {
							map[categoryName] = {};
						}
						word.category[categoryName].forEach(function(element) {
							if (!map[categoryName][element]) {
								map[categoryName][element] = [];
							}
							map[categoryName][element].push(word.word.toLowerCase());
						});
					}
				}
				if (!word.category  /* || !word.category.color */ ) {
					GeneratorUtilities.getColorCodeList().forEach(function(color) {
						map.color[color].push(word.word.toLowerCase());
					});
				}
			});
		}

		return wordMaps;
	}

	var initializeWordCategories = function(wordLists) {
		for(partOfSpeech in wordList) {
			wordLists[partOfSpeech].forEach(function(word) {
				if (!word.category || !word.category.color) {
					$.extend( true, word, { category: { color: GeneratorUtilities.getColorCodeList() } } );
				}
			});
		}
	}

	var mixCategories = function(categories1, categories2) {
		var mixedCategories = {};
		if (!categories1) {
			categories1 = {};
		}
		if (!categories2) {
			categories2 = {};
		}

		var combinedCategories = Utilities.arrayUnion(Object.keys(categories1),Object.keys(categories2));
		debug(combinedCategories);

		combinedCategories.forEach(function(category) {
			mixedCategories[category] = Utilities.arrayUnion(
				categories1[category] ? categories1[category] : [], 
				categories2[category] ? categories2[category] : []);
		});

		return mixedCategories;
	}

	var categoryMatches = function(word, card) {
		var categories = [];

		for (categoryName in card.categories) {
			if (categoryName in word.category) {
				var category = card.categories[categoryName];
				category.forEach(function(element) {
					if (word.category[categoryName].indexOf(element.toLowerCase()) != -1 && categories.indexOf(categoryName.toLowerCase()) == -1) {
						categories.push(categoryName);
					}
				});
			}
		}

		return categories;
	};

	this.getRandomNoun = getRandomNoun;
	this.getRandomAdjective = getRandomAdjective;
	this.getRandomVerb = getRandomVerb;
	this.capitalizeWord = capitalizeWord;
	this.buildWordMaps = buildWordMaps;
	this.mixCategories = mixCategories;	
	this.categoryMatches = categoryMatches;
	this.initializeWordCategories = initializeWordCategories;
	return this;
}();