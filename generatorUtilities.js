var GeneratorUtilities = function() {

	var costFactors = {
		processCountables: { variability : 0.0 }
	};

	var colorfyCost = function(cmc, color, colorDiscountFactor, colorfyFunction) {
 		var colorWeights = {
  			0: { min: 0, 	max: 0 },			
 			1: { min: 1, 	max: 1 },
 			2: { min: 1, 	max: 1.525 },
 			3: { min: 1, 	max: 1.55 },
 			4: { min: 1, 	max: 2 },
  			5: { min: 1, 	max: 2.51 },
 			6: { min: 1.33, max: 2.55 },
 			7: { min: 1.33, max: 2.66 }, 
  			8: { min: 1.4, 	max: 2.8 } 							
 		};

 		colorWeights.getNumColoredSymbols = function(cmc) {
 			var range = colorWeights[cmc] ? colorWeights[cmc] : { min: 1/cmc, max: cmc/2.5 };
 			return Math.round(Utilities.randomNumber(range.max, range.min));
 		}

 		//TODO: refactor this to be more modular somehow
 		if (colorfyFunction) {
 			return colorfyFunction();
 		}

 		if (cmc === 0) {
 			return addParameterTags('0');
 		}

 		var  numColoredSymbols = Math.max(1, colorWeights.getNumColoredSymbols(Math.floor(cmc)));
 		if (Math.round(cmc) > 2 && Utilities.randomNumber(1) < colorDiscountFactor) {
 			cmc -= Math.floor(numColoredSymbols/2);
 		}
 		cmc = Math.max(1, Math.round(cmc));

		var colorlessPart = (cmc - numColoredSymbols) > 0 ? addParameterTags(cmc - numColoredSymbols) : '';
		return encodeManaCost(colorlessPart, numColoredSymbols, color);
	}

	var colorfyXCost = function(cmc, color, variabilityCost) {
		var numColoredSymbols = Math.floor(Math.min(2, Math.max(variabilityCost, cmc / 3) + 1));			
		return GeneratorUtilities.encodeManaCost(addParameterTags('X'), numColoredSymbols, color);
	}	

	var encodeManaCost = function(colorlessPart, numColoredSymbols, color) {
		return colorlessPart + Array(numColoredSymbols + 1).join(addParameterTags(color.code));
	}

	var addParameterTags = function(parameter) {
		return '{' + parameter + '}';
	}

	var getParameterTags = function(string) {
		var regEx = new RegExp('\{(.*?)\}', 'g');

		if (!string.match(regEx)) {
			return [];
		} else {
			return string.match(regEx);
		}

	}

	var stripParameterTags = function(parameter) {
		var regEx = new RegExp('\{(.*?)\}');
		if (!parameter.match(regEx)) {
			return parameter;
		}
		return parameter.match(regEx)[1];		
	}

	var getMatchingCountables = function(dependsOn, color, isEnumerable) {
		var countableList = countables[dependsOn];

		var matchingCountables = [];

		countableList.forEach(function(countable) {
			if (countable.colors.indexOf(color.code) != -1 && (countable.enumerable || !isEnumerable)) {
				matchingCountables.push(countable);
			}
		});

		return matchingCountables;
	};

	var getMatchingCountable = function(dependsOn, color, isEnumerable) {
		var matchingCountables = getMatchingCountables(dependsOn, color, isEnumerable);
		if (matchingCountables.length == 0) {
			throw new Error('No matching countables found for ' + dependsOn + ',' + color.name);
		}

		return matchingCountables[randomInt(matchingCountables.length)];
	};

	var processCountables = function(card) {
		if (!card.countable) {
			return;
		}
		var dependsOn = Utilities.randomElement(card.countable.dependsOn);
		var countable = getMatchingCountable(dependsOn, card.color, card.countable.isEnumerable);

		var replacementText = card.countable.isEnumerable ? countable.enumerable.text : countable.text;

		card.text = Utilities.replaceParameter(card.text, substitutionCodes.countable, replacementText);
		card.cmc = card.cmc * countable.powerFactor * Utilities.randomNumber(1 + costFactors.processCountables.variability, 1 - costFactors.processCountables.variability);
		card.cost.disallowX = true;		
	};

	var getColorByCode = function(code) {
		for(var i = 0; i < Object.keys(colors).length; i++) {
			if (colors[Object.keys(colors)[i]].code == code) {
				return colors[Object.keys(colors)[i]];
			}
		}
		return undefined;
	};

	var getColorCodeList = function() {
		var colorCodeList = [];
		for (color in colors) {
			colorCodeList.push(colors[color].code);
		}
		return colorCodeList;
	}

	this.addParameterTags = addParameterTags;
	this.getParameterTags = getParameterTags;	
	this.stripParameterTags = stripParameterTags;
	this.encodeManaCost = encodeManaCost;
	this.colorfyCost = colorfyCost;
	this.colorfyXCost = colorfyXCost;
	this.processCountables = processCountables;
	this.getMatchingCountables = getMatchingCountables;
	this.getColorByCode = getColorByCode;	
	this.getColorCodeList = getColorCodeList;

	return this;
}();