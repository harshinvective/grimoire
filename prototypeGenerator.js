var SpellGenerator = function() {
	var effectIdOffset = 100;

	var generatorParameters = {
		xLikelihood: 0.08
	}

	var programData = {};
	var init = function(initData) {
		programData.spellMatrix = [];
		initData.spellMatrices.forEach(function(spellMatrix) {
			spellMatrix.forEach(function (spellEffect) {
				addNewSpellEffect(programData.spellMatrix, spellEffect);
			});
		});
		programData.effectsMap = generateEffectIds(programData.spellMatrix);
		buildColorAbilityMap(creatureAbilities);
		//WordUtilities.initializeWordCategories(wordList);
	}

	var numberWordMap = [ 'zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve'];

	function Color(name, code, creatureTypes) {
		this.name = name;
		this.code = code;
		this.creatureTypes = creatureTypes.slice(0);
	};
	var colors = {
		white:  new Color('white', 'W', ['Soldier', 'Spirit', 'Cat']),
		blue:   new Color('blue',  'U', ['Illusion', 'Squid', 'Merfolk']),
		black:  new Color('black', 'B', ['Zombie', 'Rat']),
		red:    new Color('red',   'R', ['Elemental', 'Goblin']),
		green:  new Color('green', 'G', ['Saproling', 'Wolf', 'Beast', 'Elemental'])
	};

	var substitutionCodes = {
		color: 					'COLOR',	
		tokenCreatureType: 		'TOKEN_CREATURE_TYPE',
		creatureAbility: 		'CREATURE_ABILITY',
		cardClass:  			'CARD_CLASS',
		scalableNumber: 		'N',
		countable:          	'COUNTABLE',
		objectProducingAction:  'OBJECT_PRODUCING_ACTION' 
	};

	var addNewSpellEffect = function(spellMatrix, spellEffect) {
		spellMatrix.push(spellEffect);
	}

	// initialization
	var buildColorAbilityMap = function(colorAbilityData) {
		for (var color in colors) { 
			colors[color]['creatureAbilities'] = [];
		};

		colorAbilityData.forEach(function(ability) {
			var abilityEntry = { ability: ability.keyword };
			ability.colors.forEach(function(colorData) {
				var color= GeneratorUtilities.getColorByCode(colorData.color).name;
				colors[color].creatureAbilities.push( { ability: abilityEntry, affinity: colorData.affinity, isBuff: colorData.isBuff } );
			});
		});
	};

	var nudgeCost = function(cost, costVariability) {
		var differentialCost = 0;
		var likelihood = costVariability.likelihood;

		if (nearestHalf(cost) < 1) {
			likelihood += .25;
		}

		if (randomNumber(1) < likelihood) {
			cost += randomNumber(costVariability.max, costVariability.min);
		}

		return cost;
	};

	//TODO: generalize this
	var spinCreatureEffectWheel = function(color) {
		var abilityWheel = [];
		var spinRange = 0.0;
		colors[color.name].creatureAbilities.forEach(function(abilityEntry) {
			if (abilityEntry.isBuff) {
				abilityWheel.push({ability: abilityEntry.ability.ability, threshold: spinRange + parseFloat(abilityEntry.affinity)	});
				spinRange += parseFloat(abilityEntry.affinity);
			}
		});

		var spinResult = randomNumber(spinRange, 0);
		for (var i = 0; i < abilityWheel.length; i++) {
			if (abilityWheel[i].threshold > spinResult) {
				return abilityWheel[i].ability;
			}
		};
	};

	//build cardClass map on initialization instead
	var getCardClasses = function(color) {
		var matchingCardClasses = [];

		objectClasses['card'].forEach(function(cardClass) {
			if (cardClass.colors.indexOf(color.code) != -1) {
				matchingCardClasses.push(cardClass);
			}
		});

		return matchingCardClasses;
	}

	var replaceEffectParameters = function(card) {
		//replace creature abilities (this is for spells that grant creature buffs, needs reworking)
		if (card.text.match(getStringParamRegex(substitutionCodes.cardClass))) {
			card.text = Utilities.replaceParameter(card.text, substitutionCodes.cardClass, Utilities.randomElement(getCardClasses(card.color)).text);				
		}

		//replace creature type
		if (card.text.match(getStringParamRegex(substitutionCodes.tokenCreatureType))) {
			card.text = Utilities.replaceParameter(card.text, substitutionCodes.tokenCreatureType, randomElement(card.color.creatureTypes));			
		}

		//replace color words
		if (card.text.match(getStringParamRegex(substitutionCodes.color))) {
			card.text = Utilities.replaceParameter(card.text, substitutionCodes.color, card.color.name);				
		}

		//replace creature abilities (this is for spells that grant creature buffs, needs reworking)
		if (card.text.match(getStringParamRegex(substitutionCodes.creatureAbility))) {
			card.text = Utilities.replaceParameter(card.text, substitutionCodes.creatureAbility, spinCreatureEffectWheel(card.color));				
		}		
	};

	var textFixHacks = function(card) {
		card.text = card.text.replace(new RegExp('1 cards', 'g'), 'a card');
		var numeralMatches = card.text.match(new RegExp('\\w+\\s*[^\\+\\-/](\\d+\\s+\\w+?\\b)', 'g')) || card.text.match(new RegExp('(\\w+\\s+\\d+[^/])', 'g'));
		if (numeralMatches) {
			numeralMatches.forEach(function(match) { 
				if (!match.match('(\\d+)\\s+(damage|life)') && !match.match('cost\\s+(\\d+)') && !match.match(new RegExp('scry\\s+\\d+', 'i')) ) { 
					var numeral = match.match('\\d+');
					card.text = card.text.replace(match, match.replace(numeral, numberWordMap[parseInt(numeral)]));
				}
			});
		}

		var vowelMatches = card.text.match(new RegExp('a\\s+([aeiou])', 'gi'))
		if (vowelMatches) {
			vowelMatches.forEach(function(match) {
				card.text = card.text.replace(match, match.slice(0, -1).replace('a ', 'an ') + match.slice(match.length - 1));
			});
		}
	};

	var determineSpellType = function(card) {
		var instantCostVariability = {
			min: .5,
			max: 1.4,
			likelihood: 1
		};

		var sorceryAffinity = card.sorceryAffinity || card.sorceryAffinity === 0 ? card.sorceryAffinity : card.baseCard.sorceryAffinity;

		if (randomNumber(1) > sorceryAffinity) {
			card.type = 'Instant';
		} else {
			card.type = 'Sorcery';
		}
		return card;
	};

	var getRandomBaseSpellEffect = function(spellMatrix) {
		if (!spellMatrix) {
			spellMatrix = programData.spellMatrix;
		}
		return spellMatrix[randomInt(spellMatrix.length)];
	};

	var calculateScalableValues = function(card) {
		var regEx = new RegExp('\\{' + substitutionCodes.scalableNumber + '\\}', 'g');
		if (card.text.match(regEx)) {
			var variability = card.variability || card.baseCard.variability;
			if (!variability || !variability.baseValue || !variability.deviation || !variability.variabilityCost) {
				throw new Error('Card missing expected variability data: ' + JSON.stringify(card));
			}

			var baseValue = variability.baseValue || card.baseCard.variability.baseValue;
			var baseDeviation = variability.deviation;
			var variabilityCost = variability.variabilityCost;
			if (!card.cost.disallowX && Utilities.randomNumber(1) < generatorParameters.xLikelihood) {
				card.cost.colorfyFunction = GeneratorUtilities.colorfyXCost;
	 			card.cmc = card.cmc + variabilityCost * (baseDeviation + 1.5 - baseValue);
				card.text = card.text.replace(regEx, 'X');
				card.cost.hasX = true;	
			} else {
				var deviation = roundRandom(randomNumber(baseDeviation, -baseDeviation));

				if (baseValue + deviation < 1) {
					deviation = 0;
				}

				var differentialCost = deviation * variabilityCost;	
				card.cmc += differentialCost;
				card.text = card.text.replace(regEx, baseValue + deviation);
			}
			
		}
		return card;
	};

	function getRandomSpellEffect(color, spellMatrix) {
		// change to constructor that only copies relevant bits
		var retries = 0;
		var maxRetries = 99;
		var baseCard = getRandomBaseSpellEffect(spellMatrix);	
		while (color && baseCard.colors.indexOf(color.code) == -1 && retries < maxRetries) {
			baseCard = getRandomBaseSpellEffect(spellMatrix);
			retries++;
		}
		var card = {};
		card.color = color && baseCard.colors.indexOf(color.code) != -1 ? color: GeneratorUtilities.getColorByCode(randomElement(baseCard.colors));
		card.cost = card.cost || {};
		card.cost.color = card.color;

		// TODO: once multicolor supported, add all the colors
		card.categories =  baseCard.categories;
		$.extend( true, card, { categories: { color: [card.color.code] } } );

		card.text = baseCard.ability;
		if (baseCard.countable) {
			card.countable =  $.extend({}, baseCard.countable);
		}
		card.cmc = baseCard.cmc;
		card.modAffinity = baseCard.modAffinity;	
		card.colorWeight = baseCard.colorWeight;		
		card.effectId = baseCard.effectId;
		card.baseCard = baseCard;

		if (card.baseCard.generatorFunction) {
			GeneratorFunctions[card.baseCard.generatorFunction](card);
		}

		determineSpellType(card);
		processCountables(card);
		
		return card;
	};

	var adjustCost = function(card) {
		if (card.cost.adjustCostFunction) {
			return card.cost.adjustCostFunction();
		}

		var costVariability = {
			min: -1,
			max: 1,
			likelihood: 0
		};

		var instantCostVariability = {
			min: .5,
			max: 1.4,
			likelihood: 1
		};

		// nudge cards without specific variability
		if (!card.baseCard.text == card.text) {
			card.cmc = nudgeCost(card.cmc, costVariability);
		}

		if (card.type =='Instant') {
			card.cmc = nudgeCost(card.cmc, instantCostVariability);
		}

		return card;
	};

	var addSpellModifiers = function(card, modifierFunctions) {
		for (var i = 0; i < modifierFunctions.length; i++) {
			var modifier = modifierFunctions[i](card);
			card.cmc += modifier.cost;
			card.text += '\n' + modifier.text;
			card.reminderText = modifier.reminderText;
		}
		return card;
	};

	// if spell cmc < 1, always add a modifier (otherwise it's not a full spell)
	var addSpellModifierRandom = function(card, modifiers, defaultModFactor) {
		var modFactor = defaultModFactor;
		if (card.cmc < 1) {
			modFactor = 1;
		} else if (card.modAffinity) {
			modFactor = card.modAffinity;
		}
		if (randomNumber(1) < modFactor) {
			var filteredMods =  filterModifiers(modifiers, card);
			if (Object.keys(filteredMods).length > 0) {
				addSpellModifiers(card, [randomElement(filteredMods).getMods]);
			}
		}
	};

	var generateEffectIds = function(effectsArray) {
		var effectsMap = {};
		effectsArray.forEach(function(effect, index) {
			var id = index + effectIdOffset;
			effectsMap[id] = effect;
			effect.effectId = id;		
		});
		return effectsMap;
	};

	var getEffectById = function(effectId, effectsMap) {
		if (!effectsMap) {
			effectsMap = programData.effectsMap;
		}
		return effectsMap[effectId];
	};

	function listEffectIds() {
		console.log('Effect Ids:');
		for (effect in programData.effectsMap) {
			console.log(effect + ': ' + programData.effectsMap[effect].ability);
		}
	};

	/*
		example: 
		filters:
			{
				colors: [ "U", "B" ]
				modifiers: [ "flashback" ],
				effectIds: [ 135, 201, 212 ]
			}
	*/
	function generateCard(filters) {
		var defaultModFactor = .3;
		var defaultColorDiscountFactor = .5;

		var colors = [];
		if (filters && filters.colors) {
			colors = filters.colors;
		}
	 	var color = filters.colors ? GeneratorUtilities.getColorByCode([randomElement(colors).toUpperCase()]) : undefined;

	 	// list of base spell effects to use. if not speified in filter, use the full list
	 	var spellMatrix = [];
	 	if (filters && filters.effectIds) {
	 		filters.effectIds.forEach(function(effectId) {
	 			if (programData.effectsMap[effectId]) {
	 				spellMatrix.push(programData.effectsMap[effectId]);
	 			}
	 		});
	 	}
	 	if (spellMatrix.length == 0) {
	 		spellMatrix = programData.spellMatrix;
	 	}

		var card = getRandomSpellEffect(color, spellMatrix);

		var modifiers = {};
		if (filters && filters.modifiers) {
			modifiers = getModifiersByName(filters.modifiers);
		}
		if (Object.keys(modifiers).length > 0) {
			card.modAffinity = 1;
		}	else {
			modifiers = spellModifiers;
		}
		calculateScalableValues(card);		
		addSpellModifierRandom(card, modifiers, defaultModFactor);
		adjustCost(card);
		
		var colorfyFunction = card.cost.colorfyFunction ? card.cost.colorfyFunction : GeneratorUtilities.colorfyCost;
		card.cost = colorfyFunction(card.cmc, card.cost.color, card.baseCard.colorDiscountFactor || card.baseCard.colorDiscountFactor === 0 ? card.baseCard.colorDiscountFactor : defaultColorDiscountFactor);

		replaceEffectParameters(card);
		textFixHacks(card);

		delete(card.cmc);
		delete(card.baseCard);

		return card;
	};

	this.init = init;
	this.colors = colors;
	this.substitutionCodes = substitutionCodes;
	this.generateCard = generateCard;
	this.getEffectById = getEffectById;
	this.nudgeCost = nudgeCost;
	this.listEffectIds = listEffectIds;
	this.addNewSpellEffect = function(spellEffect) { 
		return addNewSpellEffect(programData.spellMatrix, spellEffect);
	};

	return this;
};