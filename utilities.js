// utility functions
var Utilities = function() {
	var randomNumber = function(max, min) {
		if (!min) {
			min = 0;
		}
		return Math.random() * (max - min) + min;
	};
	this.randomNumber = randomNumber;

	var randomInt = function(max, min) {
		if (!min) {
			min = 0;
		}
		return Math.floor(Math.random() * (max - min) + min);
	};
	this.randomInt = randomInt;

	var randomElement = function(object_) {
		var array = object_;
		if (!Array.isArray(array)) {
			array = [];
			for (property in object_) {
				array.push(object_[property]);
			}
		}
		return array[Math.floor(randomNumber(array.length))];	
	};
	this.randomElement = randomElement;

	var roundRandom = function(number) {
		if (randomInt(2) == 0) {
			return Math.floor(number);
		} else {
			return Math.ceil(number);
		}
	};
	this.roundRandom = roundRandom;

	var nearestHalf = function(number) {
		return Math.round(2 * number) / 2;
	};
	this.nearestHalf = nearestHalf;

	var getStringParamRegex = function(parameter) {
		return new RegExp('\\{' + parameter + '\\}', 'g');
	};
	this.getStringParamRegex = getStringParamRegex;

	var getKeyIgnoreCase = function(obj, targetKey) {
		for (key in obj) {
			if ((targetKey || targetKey === 0) && targetKey.toString().toLowerCase() == key.toLowerCase()) {
				return key;		
			}
		}
		return undefined;
	};
	this.getKeyIgnoreCase = getKeyIgnoreCase;

	var replaceParameter = function(text, tokenCode, newValue) {
		return text.replace(getStringParamRegex(tokenCode), newValue);
	};
	this.replaceParameter = replaceParameter;	

	var containsKeyIgnoreCase = function(obj, targetKey) {
		return !(getKeyIgnoreCase === undefined);
	};
	this.containsKeyIgnoreCase = containsKeyIgnoreCase;

	var cloneObject = function(obj) {
		return jQuery.extend(true, {}, obj);
	};
	this.cloneObject = cloneObject;

	var removeElement = function(array, element) {
		if (array.indexOf(element) == -1) {
			return null;
		}
		array.splice(array.indexOf(element), 1);
		return element;
	};
	this.removeElement = removeElement;

	var arrayDifference = function(array1, array2) {
		var difference = [];
		array1.forEach(function(key) {
		    if (array2.indexOf(key) == -1) {
		        difference.push(key);
		    }
		}, this);
		return difference;
	};
	this.arrayDifference = arrayDifference;

	var arrayUnion = function(array1, array2) {
		return array1.slice().concat(arrayDifference(array2, array1));
	};
	this.arrayUnion = arrayUnion;

	return this;
}();